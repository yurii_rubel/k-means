import logic.image_processor as image_processor
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from PyQt5.QtWidgets \
    import QDialog, QApplication, QPushButton, QLineEdit, \
    QVBoxLayout, QSpinBox, QHBoxLayout, QLabel, QFileDialog
from PyQt5 import QtCore
import sys
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use("Qt5agg")

class Window(QDialog):
    figure_size = 50

    def __init__(self, parent=None):
        super(Window, self).__init__(parent)
        self.setWindowFlag(QtCore.Qt.WindowMinimizeButtonHint, True)
        self.setWindowFlag(QtCore.Qt.WindowMaximizeButtonHint, True)
        self.setWindowTitle('Програма кластеризації К-середніх до пікселів зображення')
        self.figure = plt.figure(figsize=(self.figure_size, self.figure_size))
        self.canvas = FigureCanvas(self.figure)
        self.toolbar = NavigationToolbar(self.canvas, self)
        self.processButton = QPushButton('Запустити')
        self.processButton.clicked.connect(self.process)
        self.input_K = QSpinBox()
        self.input_K.setValue(10)
        self.input_K.setMinimum(1)
        self.input_Attempts = QSpinBox()
        self.input_Attempts.setValue(1)
        self.input_Attempts.setMinimum(1)
        self.input_Attempts.setMaximum(25)
        self.input_path = QLineEdit()
        self.input_path.setReadOnly(True)

        hr_layout = QHBoxLayout()
        hr_layout.input_K_label = QLabel()
        hr_layout.input_K_label.setText('K (кількість кластерів): ')
        hr_layout.addWidget(hr_layout.input_K_label)
        hr_layout.addWidget(self.input_K)
        hr_layout.input_Attempts_label = QLabel()
        hr_layout.input_Attempts_label.setText('Кількість виконань: ')
        hr_layout.addWidget(hr_layout.input_Attempts_label)
        hr_layout.addWidget(self.input_Attempts)
        hr_layout.input_path_btn = QPushButton('Вибрати зображення')
        hr_layout.input_path_btn.clicked.connect(self.openFileNameDialog)
        hr_layout.addWidget(hr_layout.input_path_btn)
        hr_layout.addWidget(self.input_path)

        layout = QVBoxLayout()
        layout.addWidget(self.toolbar)
        layout.addLayout(hr_layout)
        layout.addWidget(self.processButton)
        layout.addWidget(self.canvas)
        self.setLayout(layout)

    def openFileNameDialog(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getOpenFileName(
            self,
            "Вибір зображення",
            "./images",
            "All files(*);;JPEG (*.jpg *.jpeg);;TIFF (*.tif);;PNG (*.png)",
            options=options)
        self.input_path.setText(fileName)
        self.original_img = image_processor.read_image(fileName)
        self.vectorized_img = image_processor.vectorize(self.original_img)
        self.figure.clear()
        plt.subplot(1, 2, 1),
        plt.imshow(self.original_img)
        plt.title('Оригінальне зображення'),
        plt.xticks([]), plt.yticks([])
        self.canvas.draw()

    def process(self):
        attempts = self.input_Attempts.value()
        for K in range(1, self.input_K.value() + 1):
            result_img = image_processor.process(self.vectorized_img, self.original_img, K, attempts)
            plt.subplot(1, 2, 2),
            plt.imshow(result_img)
            plt.title(f'Сегментоване зображення коли K = {K} і Кількість виконань = {attempts}'),
            plt.xticks([]), plt.yticks([])
            self.canvas.draw()
            self.canvas.start_event_loop(0.05 if K < 8 else 0.005)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    main = Window()
    main.show()
    sys.exit(app.exec_())
