import numpy as np
import cv2

def read_image(path):
    return cv2.cvtColor(cv2.imread(path), cv2.COLOR_BGR2RGB)

def vectorize(img):
    vectorized_img = img.reshape((-1, 3))
    vectorized_img = np.float32(vectorized_img)

    return vectorized_img

def process(vectorized_img, original_img, K, attempts):
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
    _, label, center = cv2.kmeans(vectorized_img, K, None, criteria, attempts, cv2.KMEANS_PP_CENTERS)
    center = np.uint8(center)
    res = center[label.flatten()]
    result_img = res.reshape(original_img.shape)

    return result_img
